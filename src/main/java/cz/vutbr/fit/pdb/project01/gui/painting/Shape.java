package cz.vutbr.fit.pdb.project01.gui.painting;

import cz.vutbr.fit.pdb.project01.gui.painting.enums.EnumColor;

import java.awt.*;

/**
 * Created by Karel Píč on 23.11.2016.
 */
public abstract class Shape
{
    protected double x1,y1,x2,y2;
    protected EnumColor color;
    protected boolean isSave;


    //konstruktory
    public Shape()
    {
    }

    public Shape(double x1, double y1, double x2, double y2, EnumColor color)
    {
        setX1(x1);
        setY1(y1);
        setX2(x2);
        setY2(y2);
        setEnumColor(color);
        setIsSave(false);
    }
    //funkce


    public void setX1(double x1)
    {
        this.x1 = x1;
    }

    public void setY1(double y1)
    {
        this.y1 = y1;
    }

    public void setX2(double x2)
    {
        this.x2 = x2;
    }

    public void setY2(double y2)
    {
        this.y2 = y2;
    }

    public void setEnumColor(EnumColor color)
    {
        this.color = color;
    }

    public void setIsSave(boolean isSave)
    {
        this.isSave = isSave;
    }


    public double getX1()
    {
        return this.x1;
    }

    public double getY1()
    {
        return this.y1;
    }

    public double getX2()
    {
        return this.x2;
    }

    public double getY2()
    {
        return this.y2;
    }

    public EnumColor getEnumColor()
    {
        return this.color;
    }

    public boolean getIsSave()
    {
        return this.isSave;
    }

    public abstract void draw(Graphics2D g2);

    public abstract void addPoint(double x, double y);
}
