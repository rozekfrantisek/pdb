package cz.vutbr.fit.pdb.project01.gui.listeners;

import cz.vutbr.fit.pdb.project01.gui.Zoo;
import cz.vutbr.fit.pdb.project01.gui.painting.enums.EnumColor;
import cz.vutbr.fit.pdb.project01.gui.painting.enums.EnumShape;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Karel Píč on 25.11.2016.
 */
public class ListenersInfoPanel implements ActionListener
{
	Zoo zoo;
	// comboBox pro tvary a barvy
	JComboBox comboBox;
	JTextArea textArea;

	public ListenersInfoPanel(Zoo zoo, JComboBox comboBox, JTextArea textArea){
		this.zoo = zoo;
		this.comboBox = comboBox;
		this.textArea = textArea;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Událost posluchače: " + e.getActionCommand());

		switch (e.getActionCommand()) {
			case "comboBoxChanged":
				this.comboBoxChanged(e);
		}
	}

	public void comboBoxChanged(ActionEvent e)
	{
		String newMode = (String)this.comboBox.getSelectedItem();
		/*if (newMode.equals(EnumShape.LINE.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení čáry.");
			this.textArea.setText(EnumShape.LINE.getAdvice());
			this.zoo.getInteractivePanel().setShapeType(EnumShape.LINE);
		} else*/ if (newMode.equals(EnumShape.FENCEELLIPSE.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení elipsy.");
			this.textArea.setText(EnumShape.FENCEELLIPSE.getAdvice());
			this.zoo.getInteractivePanel().setShapeType(EnumShape.FENCEELLIPSE);
		} else if (newMode.equals(EnumShape.FENCEOVAL.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení ovál.");
			this.textArea.setText(EnumShape.FENCEOVAL.getAdvice());
			this.zoo.getInteractivePanel().setShapeType(EnumShape.FENCEOVAL);
		} else if (newMode.equals(EnumShape.FENCEPOLYGON.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení polygon.");
			this.textArea.setText(EnumShape.FENCEPOLYGON.getAdvice());
			this.zoo.getInteractivePanel().setShapeType(EnumShape.FENCEPOLYGON);
		} else if (newMode.equals(EnumShape.FENCERECTANGLE.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení obdelníku.");
			this.textArea.setText(EnumShape.FENCERECTANGLE.getAdvice());
			this.zoo.getInteractivePanel().setShapeType(EnumShape.FENCERECTANGLE);
		}

		if (newMode.equals(EnumColor.FENCE.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: výplň pro ohrady.");
			this.zoo.getInteractivePanel().setShapeColor(EnumColor.FENCE);
		} else if (newMode.equals(EnumColor.GRASS.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení ovál.");
			this.zoo.getInteractivePanel().setShapeColor(EnumColor.GRASS);
		} else if (newMode.equals(EnumColor.ROAD.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení polygon.");
			this.zoo.getInteractivePanel().setShapeColor(EnumColor.ROAD);
		} else if (newMode.equals(EnumColor.WATER.getName())) {
			this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení obdelníku.");
			this.zoo.getInteractivePanel().setShapeColor(EnumColor.WATER);
		}
	}
}
