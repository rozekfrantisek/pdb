package cz.vutbr.fit.pdb.project01.gui.painting.enums;

/**
 * Created by Karel Píč on 25.11.2016.
 */
public enum EnumShape {
	//LINE("Čára","Stisknutím levého tlačítka zadáte počteční bod, posunem a uvolněním levého talčítka zadáte koncový bod.") ,
	FENCERECTANGLE("Obdelník","Stisknutím levého tlačítka zvolíte počáteční bod, velikost volíte posunem myši a uvolněním levého talčítka uložíte Obdelník."),
	FENCEPOLYGON("Polygon","Stisknutím levého tlačítka přidáváte body. Pravým tlačítkem ukládáte polygon."),
	FENCEELLIPSE("Kružnice","Stisknutím levého tlačítka zvolíte počáteční bod, posunem a uvolněním nastavíte velikost a uložíte kružnici."),
	FENCEOVAL("Ovál","Stisknutím levého tlačítka zvolíte počáteční bod, posunem a uvolněním nastavíte velikost a uložíte ovál.");

	private String name;
	private String advice;

	EnumShape(String name, String advice)
	{
		this.name = name;
		this.advice = advice;
	}

	public String getAdvice() {
		return advice;
	}

	public String getName() {
		return name;
	}

	public String toString()
	{
		return this.name;
	}
}
