package cz.vutbr.fit.pdb.project01.gui.painting;

import cz.vutbr.fit.pdb.project01.gui.painting.enums.EnumColor;

import java.awt.*;
import java.util.Arrays;

/**
 * Created by Karel Píč on 25.11.2016.
 */
public class FencePolygon extends Shape {

    private int[] xPointsPolygon;
    private int[] yPointsPolygon;
    //konstruktor

    public FencePolygon()
    {
        // volani zdedeneho konstruktoru
        super();
        this.xPointsPolygon = new int[]{};
        this.yPointsPolygon = new int[]{};
        super.isSave = false;
    }

    public FencePolygon(double x1, double y1, double x2, double y2, EnumColor color)
    {
        this.xPointsPolygon = new int[]{};
        this.yPointsPolygon = new int[]{};
        this.addPoint(x1, y1);
        this.addPoint(x2, y2); // druhy bod kvuli tomu aby tam bylo na zacatku videt tecka
        super.setEnumColor(color);
        super.isSave=false;
    }

    //funkce
    static int[] addElement(int[] a, int e)
    {
        a  = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }

    @Override
    public void addPoint(double x, double y)
    {
        System.out.println("Přidávám bod x y");
        this.xPointsPolygon = this.addElement(this.xPointsPolygon,(int) x);
        this.yPointsPolygon = this.addElement(this.yPointsPolygon,(int) y);
        this.setX1(x);
        this.setY1(y);
        this.setX2(x);
        this.setY2(y);
    }

    @Override
    public void draw(Graphics2D g2)
    {
        System.out.println(Arrays.toString(this.xPointsPolygon));
        if (!super.isSave) {
            int x[] = this.xPointsPolygon.clone();
            int y[] = this.yPointsPolygon.clone();
            x = this.addElement(x, (int) this.getX2());
            y = this.addElement(y, (int) this.getY2());
            g2.setColor(super.getEnumColor().getBoardColor());
            g2.draw(new Polygon(x, y, x.length));
        } else {
            g2.setColor(super.getEnumColor().getBackgroundColor());
            g2.fill(new Polygon(this.xPointsPolygon, this.yPointsPolygon, this.xPointsPolygon.length));
            g2.setColor(super.getEnumColor().getBoardColor());
            g2.setStroke(super.getEnumColor().getBorder());
            g2.draw(new Polygon(this.xPointsPolygon, this.yPointsPolygon, this.xPointsPolygon.length));
        }
    }
}
