package cz.vutbr.fit.pdb.project01.gui.painting;

import cz.vutbr.fit.pdb.project01.gui.painting.enums.EnumColor;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Created by Karel Píč on 24.11.2016.
 */
public class FenceRectangle extends Shape {

    //konstruktor

    public FenceRectangle()
    {
        // volani zdedeneho konstruktoru
        super();
    }

    public FenceRectangle(double x1, double y1, double x2, double y2, EnumColor color)
    {
        super(x1, y1, x2, y2, color);
    }

    //funkce

    public void draw(Graphics2D g2)
    {
        double x,y,width,height;
        x = (super.getX1() > super.getX2())? super.getX2(): super.getX1();
        y = (super.getY1() > super.getY2())? super.getY2(): super.getY1();
        width = Math.abs(super.getX1() - super.getX2());
        height = Math.abs(super.getY1() - super.getY2());
        g2.setColor(super.getEnumColor().getBackgroundColor());
        g2.fill(new Rectangle2D.Double(x, y, width, height));
        g2.setStroke(super.getEnumColor().getBorder());
        g2.setColor(super.getEnumColor().getBoardColor());
        g2.draw(new Rectangle2D.Double(x, y, width, height));
    }

    @Override
    public void addPoint(double x, double y) {

    }
}
