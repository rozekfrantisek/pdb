package cz.vutbr.fit.pdb.project01.gui.painting.enums;

import java.awt.*;

/**
 * Created by Karel Píč on 25.11.2016.
 */
public enum EnumColor {
	WATER(new Color(41, 109, 156),new Color(41, 50, 156), new BasicStroke(4), "Vodní plocha") ,
	GRASS(new Color(71, 156, 38), new Color(71, 156, 38), new BasicStroke(0),"Travnaté plochy"),
	ROAD(new Color(0, 0, 0), new Color(94, 94, 108), new BasicStroke(2), "Komunikace"),
	FENCE(new Color(156, 92, 83), new Color(156, 118, 106), new BasicStroke(2), "Ohrady");

	private String name;
	private Color boardColor;
	private Color backgroundColor;
	private Stroke border;

	EnumColor(Color boardColor, Color backgroundColor,Stroke border, String name)
	{
		this.name = name;
		this.boardColor = boardColor;
		this.backgroundColor = backgroundColor;
		this.border = border;
	}

	public Color getBoardColor()
	{
		return this.boardColor;
	}

	public Color getBackgroundColor()
	{
		return this.backgroundColor;
	}

	public String getName()
	{
		return this.name;
	}

	public Stroke getBorder()
	{
		return this.border;
	}

	public String toString()
	{
		return this.name;
	}
}