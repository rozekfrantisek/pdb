package cz.vutbr.fit.pdb.project01.gui;

import cz.vutbr.fit.pdb.project01.gui.listeners.ListenersMenuBar;
import cz.vutbr.fit.pdb.project01.gui.threads.Threads;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Karel Píč on 05.10.2016.
 */
public class Zoo extends JFrame {
    DescriptionPanel descriptionPanel;
    InteractivePanel interactivePanel;
    InfoPanel infoPanel;
    JMenuBar menubar;

    Threads threads;

    //externí okno
    private JFileChooser chooser = new JFileChooser();

    /* KONSTRUKTORY */
    public Zoo(int width, int height)
    {

        int heightDescriptionPanel = 170;
        int widthInfoPanel = 200;
        this.descriptionPanel = new DescriptionPanel(width, heightDescriptionPanel);
        this.interactivePanel = new InteractivePanel(this, width - widthInfoPanel, height - heightDescriptionPanel);
        this.infoPanel = new InfoPanel(this, widthInfoPanel, height - heightDescriptionPanel);

        this.threads = new Threads(this);
    }

    /* FUNKCE */

    public void run()
    {
        System.out.println("Spuštění aplikace Zoo.");
        startGui();
    }

    public void startGui()
    {
        this.setTitle("Správa Zoo");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout(5, 5));

        Box interactivePanelBox = Box.createHorizontalBox();
        this.add(interactivePanelBox, BorderLayout.CENTER);
        interactivePanelBox.add(this.interactivePanel);

        Box descriptionPanelBox = Box.createHorizontalBox();
        this.add(descriptionPanelBox, BorderLayout.SOUTH);
        descriptionPanelBox.add(this.descriptionPanel);

        Box infoPanelBox = Box.createHorizontalBox();
        this.add(infoPanelBox, BorderLayout.EAST);
        infoPanelBox.add(this.infoPanel);

        this.setJMenuBar(setMenu());

        this.setVisible(true);
        this.pack();
    }

    public JMenuBar setMenu()
    {
        JMenu menu;
        JMenuItem menuItem;
        ListenersMenuBar listener;

        this.menubar = new JMenuBar();
        //menu Zoo
        menu = new JMenu("Zoo");
        //nové zoo
        menuItem = new JMenuItem("Nové zoo");
        menuItem.setMnemonic(KeyEvent.VK_H);
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        //zpět
        menuItem = new JMenuItem("Zpět");
        menuItem.setMnemonic(KeyEvent.VK_Z);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,ActionEvent.CTRL_MASK));
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menuItem.setEnabled(false);
        menu.add(menuItem);
        menu.addSeparator();
        //uložit zoo
        menuItem = new JMenuItem("Uložit zoo");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.CTRL_MASK));
        menuItem.setEnabled(false);
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        //načíst zoo
        menuItem = new JMenuItem("Načíst zoo");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L,ActionEvent.CTRL_MASK));
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        //Zoo
        menuItem = new JMenuItem("Zoo");
        menuItem.setMnemonic(KeyEvent.VK_O);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.CTRL_MASK));
        menuItem.setEnabled(true);
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        //konec
        menuItem = new JMenuItem("Konec");
        menuItem.setMnemonic(KeyEvent.VK_K);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, ActionEvent.CTRL_MASK));
        menuItem.setEnabled(false);
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        menubar.add(menu);

        //režim
        menu = new JMenu("Režim");
        menuItem = new JMenuItem("Úpravy");
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        menuItem = new JMenuItem("Výběr");
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        menubar.add(menu);

        //nastaveni
        menu = new JMenu("Nastaveni");
        menuItem = new JMenuItem("Smazat zprávy");
        listener = new ListenersMenuBar(this);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        menubar.add(menu);

        return menubar;
    }

    //TODO vybrat soubor pro nahrání
    public void chooseFile(boolean mode) throws IOException, FileNotFoundException, ClassNotFoundException
    {
            if (mode) {
                this.chooser.setApproveButtonText("Uložit");
            } else {
                this.chooser.setApproveButtonText("Nahrát");
            }
            int stav = this.chooser.showSaveDialog(null);
            if ( stav == JFileChooser.APPROVE_OPTION) {
                File cesta = this.chooser.getSelectedFile();
                if (mode) {
                    boolean isFile = false;
                    if(!cesta.exists())
                        cesta.createNewFile();
                    this.saveZoo(cesta);
                } else {
                    this.loadZoo(cesta);
                }
            }
    }

    //TODO načtení zoo
    private void loadZoo(File cesta)
    {
    }

    //TODO uložení zoo
    private void saveZoo(File cesta)
    {
    }

    //TODO zavření
    public void closeZoo()
    {

    }

    public DescriptionPanel getDescriptionPanel()
    {
        return this.descriptionPanel;
    }

    public InteractivePanel getInteractivePanel()
    {
        return this.interactivePanel;
    }

    public InfoPanel getInfoPanel()
    {
        return this.infoPanel;
    }
}
