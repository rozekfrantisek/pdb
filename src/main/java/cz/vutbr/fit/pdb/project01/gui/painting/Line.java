package cz.vutbr.fit.pdb.project01.gui.painting;

import cz.vutbr.fit.pdb.project01.gui.painting.enums.EnumColor;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Created by Karel Píč on 24.11.2016.
 */
public class Line extends Shape {

    //konstruktor

    public Line()
    {
        // volani zdedeneho konstruktoru
        super();
    }

    public Line(double x1, double y1, double x2, double y2, EnumColor color)
    {
        super(x1, y1, x2, y2, color);
    }

    //funkce

    public void draw(Graphics2D g2)
    {
        g2.setColor(super.getEnumColor().getBoardColor());
        g2.draw(new Line2D.Double(getX1(), getY1(), getX2(), getY2()));
    }

    @Override
    public void addPoint(double x, double y) {

    }
}
