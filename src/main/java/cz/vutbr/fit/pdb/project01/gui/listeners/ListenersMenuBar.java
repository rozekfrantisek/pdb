package cz.vutbr.fit.pdb.project01.gui.listeners;

import cz.vutbr.fit.pdb.project01.gui.InfoPanel;
import cz.vutbr.fit.pdb.project01.gui.ModeType;
import cz.vutbr.fit.pdb.project01.gui.Zoo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Karel Píč on 05.10.2016.
 */
public class ListenersMenuBar implements ActionListener {
    Zoo zoo;

    public ListenersMenuBar(Zoo zoo){
        this.zoo = zoo;
    }

    //TODO jen takový nástřel - dodělat
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Událost posluchače test"+e.getActionCommand());
        switch (e.getActionCommand()) {
            case "Smazat zprávy":
	            this.zoo.getDescriptionPanel().clearInfoMessage();
	            break;
            case "Nové zoo":
                Thread novyzoo = new Thread() {
                    @Override
                    public void run() {
                        Zoo zoo = new Zoo(800, 600);
                        zoo.setVisible(true);
                    }
                };
                try {
                    novyzoo.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ListenersMenuBar.class.getName()).log(Level.SEVERE, null, ex);
                }
                novyzoo.start();
                return;

            case "Konec":
                this.zoo.closeZoo();
                this.zoo.getDescriptionPanel().setInfoMessage("Ukončili jste hru.");
                break;

            case "Uložit hru":
                this.zoo.getDescriptionPanel().setInfoMessage("Ukládání hry");
                try {
                    this.zoo.chooseFile(true);
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(ListenersMenuBar.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "Načíst hru":
                this.zoo.getDescriptionPanel().setInfoMessage("Načtení hry");
                try {
                    this.zoo.chooseFile(false);
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(ListenersMenuBar.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            case "Zpět":
                this.zoo.getDescriptionPanel().setInfoMessage("Vráceni o krok zpět.");
                break;

            //režim
            case "Úpravy":
                this.zoo.getDescriptionPanel().setInfoMessage("Změna režimu na: Úpravy.");
                this.zoo.getInfoPanel().changeMode(ModeType.EDIT);
                break;

            case "Výběr":
                this.zoo.getDescriptionPanel().setInfoMessage("Změna režimu na: Výběr.");
                this.zoo.getInfoPanel().changeMode(ModeType.INFO);
                break;

            default:
        }
    }
}
