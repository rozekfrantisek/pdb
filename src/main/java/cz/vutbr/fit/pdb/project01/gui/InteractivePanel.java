package cz.vutbr.fit.pdb.project01.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import cz.vutbr.fit.pdb.project01.gui.painting.enums.*;
import cz.vutbr.fit.pdb.project01.gui.painting.*;
import cz.vutbr.fit.pdb.project01.gui.painting.Shape;

/**
 * Created by Karel Píč on 05.10.2016.
 */
public class InteractivePanel extends JComponent implements MouseListener, MouseMotionListener
{
    private ArrayList<Shape> shapes = new ArrayList<Shape>();
    private EnumShape shapeType;
	private EnumColor shapeColor;
    private Shape currentShape;
    Zoo zoo;

    public InteractivePanel(Zoo zoo, int width, int height)
    {
        this.zoo = zoo;
        this.setPreferredSize(new Dimension(width, height));
        this.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        addListeners();

        this.shapes = new ArrayList<Shape>();
        this.shapeColor = EnumColor.WATER;
        this.currentShape = null;
        this.setShapeType(EnumShape.FENCERECTANGLE);
	    this.zoo.getDescriptionPanel().setInfoMessage("Režim úprav: kreslení obdelníku.");
    }


    public void addListeners()
    {
        addMouseMotionListener(this);
        addMouseListener(this);
    }

    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        super.paintComponent(g2);

        //System.out.println("Počet uložených elementů:"+this.shapes.size());
        for (int i=0; i<this.shapes.size(); i++) {
            this.shapes.get(i).draw(g2);
        }

        if (this.currentShape != null) {
            this.currentShape.draw(g2);
        }
    }

    //TODO doladit klikání levým a pravým
    public void mouseClicked(MouseEvent e)
    {
        System.out.println("mouseClicked");
        if (SwingUtilities.isRightMouseButton(e)) {
            System.out.println("mouseClicked - R -"+ this.getShapeType());
            switch (this.getShapeType()) {
                //case LINE:
                case FENCERECTANGLE:
	            case FENCEELLIPSE:
	            case FENCEOVAL:
                    break;
                case FENCEPOLYGON:
                    if (this.currentShape != null) {
	                    zoo.descriptionPanel.setInfoMessage("Uložil jsem Polygon");
	                    this.currentShape.setIsSave(true);
                        this.shapes.add(this.currentShape);
                    }
                    this.currentShape = null;
                    break;
            }
        }

        if (SwingUtilities.isLeftMouseButton(e)) {
	        System.out.println("mouseClicked - L "+ this.getShapeType());
            switch (this.getShapeType()) {
                //case LINE:
                case FENCERECTANGLE:
                case FENCEPOLYGON:
	            case FENCEELLIPSE:
	            case FENCEOVAL:
		            break;
            }
            repaint();
        }
    }

    /**
     * Stisknutí myši
     * @param e
     */
    public void mousePressed(MouseEvent e)
    {

        System.out.println("mousePressed");
        switch (this.getShapeType()) {
            /*case LINE:
                zoo.descriptionPanel.setInfoMessage("Natavil jsem počateční bod čáry ["+e.getX()+","+e.getY()+"]", e);
                this.currentShape = new Line(e.getX(), e.getY(), e.getX(), e.getY(), this.shapeColor);
                break;*/
            case FENCERECTANGLE:
                zoo.descriptionPanel.setInfoMessage("Natavil jsem počateční bod ohradyR ["+e.getX()+","+e.getY()+"]", e);
                this.currentShape = new FenceRectangle(e.getX(), e.getY(), e.getX(), e.getY(), this.shapeColor);
                break;
            case FENCEPOLYGON:
            	if (SwingUtilities.isLeftMouseButton(e)) {
		            if (this.currentShape != null) {
			            System.out.println("mouseReleased - L - přenastavuji současny obraz - param ("+e.getX()+"|"+e.getY()+")");
			            this.currentShape.setX2(e.getX());
			            this.currentShape.setY2(e.getY());
		            }
	            }
	            break;
	        case FENCEELLIPSE:
		        zoo.descriptionPanel.setInfoMessage("Natavil jsem počateční bod elipsy ["+e.getX()+","+e.getY()+"]", e);
		        this.currentShape = new FenceEllipse(e.getX(), e.getY(), e.getX(), e.getY(), this.shapeColor);
		        break;
	        case FENCEOVAL:
	        	zoo.descriptionPanel.setInfoMessage("Natavil jsem počateční bod oválu ["+e.getX()+","+e.getY()+"]", e);
		        this.currentShape = new FenceOval(e.getX(), e.getY(), e.getX(), e.getY(), this.shapeColor);
		        break;
        }
        repaint();
    }

    /**
     * Uvolnění stisku myši
     * @param e
     */
    public void mouseReleased(MouseEvent e)
    {
        System.out.println("mouseReleased");

        switch (this.getShapeType()) {
	        //case LINE:
	        case FENCERECTANGLE:
	        case FENCEELLIPSE:
	        case FENCEOVAL:
		        if (this.currentShape != null) {
			        System.out.println("mouseReleased - LMR - přenastavuji současny obraz - param ("+e.getX()+"|"+e.getY()+")");
			        this.currentShape.setX2(e.getX());
			        this.currentShape.setY2(e.getY());

			        this.shapes.add(this.currentShape);
			        this.currentShape = null;
		        }
		        break;
	        case FENCEPOLYGON:
		        if (SwingUtilities.isLeftMouseButton(e)) {
			        System.out.println("mouseReleased - L - přenastavuji současny obraz - param ("+e.getX()+"|"+e.getY()+")");
			        if (this.currentShape == null) {
				        zoo.descriptionPanel.setInfoMessage("Nastavil jsem počáteční body ohradyPolygon [" + e.getX() + "," + e.getY() + "]", this.getClass());
				        this.currentShape = new FencePolygon(e.getX(), e.getY(), e.getX(), e.getY(), this.shapeColor);
			        } else {
				        zoo.descriptionPanel.setInfoMessage("Přidávám body [" + e.getX() + "," + e.getY() + "]", e);
				        this.currentShape.addPoint(e.getX(), e.getY());
			        }
		        }
		        break;
        }
        repaint();
    }

    //TODO udělat, když vyjede s myší, že se zastaví vykreslování - nebude se měnit X a Y abych neměl objekty mimo v databázi
    public void mouseEntered(MouseEvent e)
    {
        System.out.println("mouseEntered");
    }

    public void mouseExited(MouseEvent e)
    {
        System.out.println("mouseReleased");
    }

    public void mouseDragged(MouseEvent e)
    {
	    System.out.println("mouseReleased");
        if(this.currentShape != null)
        {
            switch (this.getShapeType()) {
                //case LINE:
                case FENCERECTANGLE:
	            case FENCEPOLYGON:
	            case FENCEELLIPSE:
	            case FENCEOVAL:
		            if (SwingUtilities.isLeftMouseButton(e)) {
			            System.out.println("mouseDragged - přenastavuji současny obraz - param ("+e.getX()+"|"+e.getY()+")");
			            this.currentShape.setX2(e.getX());
			            this.currentShape.setY2(e.getY());
			            repaint();
		            }
		            break;
            }
        }
    }

    public void mouseMoved(MouseEvent e)
    {
    }


    public void setShapeType(EnumShape type)
    {
        this.shapeType = type;
    }

    public void setShapeColor(EnumColor color)
    {
	    this.shapeColor = color;
    }

    public EnumShape getShapeType()
    {
        return this.shapeType;
    }

    public EnumColor getShapeColor()
    {
	    return this.shapeColor;
    }


	/**
	 * Převod položek z enum do pole string[] kvůli selectoru v InfoPanelu
	 * @return
	 */
	public String[] getShapeTypesName()
	{
		//TODO možná trochu krkolomné
		ArrayList<String> enumNames = new ArrayList<String>();
		for(EnumShape shapeType : EnumShape.values()){
			enumNames.add(shapeType.toString());
		}
		String[] enumNameskArr = new String[enumNames.size()];
		enumNameskArr = enumNames.toArray(enumNameskArr);
		return enumNameskArr;
	}

	public String[] getColorName()
	{
		//TODO možná trochu krkolomné
		ArrayList<String> enumNames = new ArrayList<String>();
		for(EnumColor colorType : EnumColor.values()){
			enumNames.add(colorType.toString());
		}
		String[] enumNameskArr = new String[enumNames.size()];
		enumNameskArr = enumNames.toArray(enumNameskArr);
		return enumNameskArr;
	}
}
