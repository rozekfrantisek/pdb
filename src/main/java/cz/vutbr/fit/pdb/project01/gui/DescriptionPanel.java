package cz.vutbr.fit.pdb.project01.gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by Karel Píč on 05.10.2016.
 */
public class DescriptionPanel extends JPanel {
    static final String NEWLINE = System.getProperty("line.separator");
    private JTextArea infoMessageLabel;

    public DescriptionPanel(int width, int height){

        //info message
        this.infoMessageLabel = new JTextArea("Vítáme Vás v správci pro vaši zoo." + NEWLINE);
        this.infoMessageLabel.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(this.infoMessageLabel);
        scrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(width, height));
        add(scrollPane);

    }

    /**
     * Vypisuje výpis informace.
     * @param text Text zprávy, která mý být vypsána.
     */
    public void setInfoMessage(String text, MouseEvent e){
        //this.infoMessageLabel.setText(text);
        this.infoMessageLabel.append(text + " z funkce "
                + e.getComponent().getClass().getName()
                + "." + NEWLINE);
        this.infoMessageLabel.setCaretPosition(this.infoMessageLabel.getDocument().getLength());
    }

    public void setInfoMessage(String text, Class c)
    {
        this.infoMessageLabel.append(text + " ze třídy:"
                + c.getName()
                + " a funkce "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + "." + NEWLINE);
        this.infoMessageLabel.setCaretPosition(this.infoMessageLabel.getDocument().getLength());
    }

    public void setInfoMessage(String text){
        //this.infoMessageLabel.setText(text);
        this.infoMessageLabel.append(text + NEWLINE);
        this.infoMessageLabel.setCaretPosition(this.infoMessageLabel.getDocument().getLength());
    }

    public void clearInfoMessage() {
        this.infoMessageLabel.setText(null);
    }
}